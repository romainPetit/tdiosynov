//
//  Product.swift
//  delivrooTd
//
//  Created by romain PETIT on 01/02/2019.
//  Copyright © 2019 romain PETIT. All rights reserved.
//

import Foundation
struct Dish {
    
    enum typeDish{
        case main
        case appertizer
        case desert
        case drink
    }
    
    var name: String
    var price: Double
    var description: String
    var disponibility: Bool
//    var ingredient: [Double : String]
    var image: String
    var type: typeDish
}
