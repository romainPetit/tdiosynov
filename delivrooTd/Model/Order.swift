//
//  Order.swift
//  delivrooTd
//
//  Created by romain PETIT on 01/02/2019.
//  Copyright © 2019 romain PETIT. All rights reserved.
//

import Foundation
struct Order {
    var orderedDishes: [Dish]
    
    var totalPrice: Double {
        return 0
    }
    
    mutating func insert(dish: Dish){
        orderedDishes.append(dish)
    }
    
    mutating func remove(at index: Int){
        orderedDishes.remove(at: index)
    }
    
    
}
