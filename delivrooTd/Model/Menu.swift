//
//  Menu.swift
//  delivrooTd
//
//  Created by romain PETIT on 01/02/2019.
//  Copyright © 2019 romain PETIT. All rights reserved.
//

import Foundation
struct Menu {
    var dishes: [Dish] = []
    
    mutating func append(dish: Dish){
        dishes.append(dish)
    }
    
    mutating func insert(dish: Dish, at index: Int){
        dishes.insert(dish, at: index)
    }
    
    mutating func remove(at index: Int){
        dishes.remove(at: index)
    }
    
    func get(index: Int) -> Dish{
        let dish = dishes[index]
        return dish
        
    }
}
