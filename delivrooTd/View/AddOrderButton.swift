//
//  AddOrderButton.swift
//  delivrooTd
//
//  Created by romain PETIT on 07/02/2019.
//  Copyright © 2019 romain PETIT. All rights reserved.
//

import UIKit

class AddOrderButton: UIButton {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    var couleurSurlignement = UIColor.lightGray
    {
        didSet {
            if isHighlighted {
                backgroundColor = couleurSurlignement
            }
        }
    }
    var couleurDefault = UIColor.gray
    {
        didSet{
            if !isHighlighted{
                backgroundColor = couleurDefault
            }
        }
    }
    
    override var isHighlighted: Bool{
        didSet{
            if isHighlighted{
                backgroundColor = couleurSurlignement
            } else{
                backgroundColor = couleurDefault
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    
    func setup() {
        self.layer.cornerRadius = self.frame.height / 5
        self.clipsToBounds = true
        self.backgroundColor = UIColor.gray
        self.setTitleColor(UIColor.white, for: .normal)
        self.setTitle("add to order", for: .normal)
    }

}
