//
//  DishTableViewCell.swift
//  delivrooTd
//
//  Created by romain PETIT on 01/02/2019.
//  Copyright © 2019 romain PETIT. All rights reserved.
//

import UIKit

class DishTableViewCell: UITableViewCell {
    
    @IBOutlet weak var dishImageView: UIImageView!
    @IBOutlet weak var nameDishLabel: UILabel!
    @IBOutlet weak var priceDishLabel: UILabel!
    

    
    
    override func layoutSubviews() {
        dishImageView.layer.cornerRadius = dishImageView.bounds.height / 10
        dishImageView.clipsToBounds = true
    }
//    override func awakeFromNib() {
//        super.awakeFromNib()
//
//        // Initialization code
//    }
//
//    override func setSelected(_ selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
//
//        // Configure the view for the selected state
//    }

}
