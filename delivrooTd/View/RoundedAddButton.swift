//
//  RoundedAddButton.swift
//  delivrooTd
//
//  Created by romain PETIT on 07/02/2019.
//  Copyright © 2019 romain PETIT. All rights reserved.
//

import UIKit

class RoundedAddButton: UIButton {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
//    var couleurSurlignement = UIColor.lightGray
//    {
//        didSet {
//            if isHighlighted {
//                self.tintColor = couleurSurlignement
//            }
//        }
//    }
//    var couleurDefault = UIColor.black
//    {
//        didSet{
//            if !isHighlighted{
//                self.tintColor = couleurDefault
//            }
//        }
//    }
//
//    override var isHighlighted: Bool{
//        didSet{
//            if isHighlighted{
//                self.tintColor = couleurSurlignement
//            } else{
//                self.tintColor = couleurDefault
//            }
//        }
//    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    func setup(){
        //let origImage = UIImage(named: "plus");
        //let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
        //self.setImage(tintedImage, for: .normal)
        self.tintColor = UIColor.lightGray
    }

}
