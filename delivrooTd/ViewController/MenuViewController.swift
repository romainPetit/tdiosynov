//
//  ViewController.swift
//  delivrooTd
//
//  Created by romain PETIT on 01/02/2019.
//  Copyright © 2019 romain PETIT. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    let cellIdentifier = "dishCell"
    
    var dish1 = Dish.init(name: "Un diné de concombre", price: 12.3, description: """
Repas Bio Vegetarien
Contient de la salade, de la tomate, de l'aubergine, du concombre accompagné de sa vinégraite maison.
""", disponibility: true, image: "concombre", type: .main)
    
    var dish2 = Dish.init(name: "Asperge et de l'huile d'olives", price: 52, description: """
Repas Bio
Contient des asperges Bio, deux tranches de tomates à la provençale, accompagné d'un steak.
""", disponibility: true, image: "asperges", type: .main)
    
    var dish3 = Dish.init(name: "Avocat dans son palais de jus stice", price: 22, description: """
Repas Diététique
Contient de la salade, de la tomate, une cuisse de lapin accompagné de son avocat.
""", disponibility: true, image: "avocat", type: .main)
    
    var menu = Menu()
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menu.dishes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: DishTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! DishTableViewCell
        
        cell.dishImageView.image = UIImage(named: menu.get(index: indexPath.row).image)
        cell.nameDishLabel.text = menu.get(index: indexPath.row).name
        cell.priceDishLabel.text = "\(menu.get(index: indexPath.row).price) €"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("vous avez tapé sur le lien \(indexPath.row)")
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        
        menu.append(dish: dish1)
        menu.append(dish: dish2)
        menu.append(dish: dish3)

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "dishSegue" {
            guard let destination = segue.destination as? DishViewController else {return}
            guard let cell = sender as? UITableViewCell else {return}
            guard let indexPath = tableView.indexPath(for: cell) else {return}
            tableView.deselectRow(at: indexPath, animated: true)
            destination.currentDish = menu.get(index: indexPath.row)
        }
    }
    
    
    
    
    
    


}

