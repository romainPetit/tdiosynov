//
//  DishViewController.swift
//  delivrooTd
//
//  Created by romain PETIT on 04/02/2019.
//  Copyright © 2019 romain PETIT. All rights reserved.
//

import UIKit

class DishViewController: UIViewController {
    
    @IBOutlet weak var dishImageView: UIImageView!
    @IBOutlet weak var dishNameLabel: UILabel!
    @IBOutlet weak var dishDescriptionLabel: UILabel!
    
    @IBOutlet weak var nbDishLabel: UILabel!
    var currentDish: Dish?
    var nbDishToOrder: Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        
        nbDishLabel.text = String(nbDishToOrder)
        
        if let dishImage = currentDish?.image{
            dishImageView.image = UIImage(named: dishImage)
        }
        if let dishName = currentDish?.name{
            dishNameLabel.text = dishName
        }
        if let dishDescription = currentDish?.description{
            dishDescriptionLabel.text = dishDescription
        }
        
        
    }
    
    
    @IBAction func increaseNbDish(_ sender: Any) {
        nbDishToOrder += 1
        nbDishLabel.text = String(nbDishToOrder)
        print(nbDishToOrder)
    }
    @IBAction func decreaseNbDish(_ sender: Any) {
        if(nbDishToOrder <= 0){
            nbDishToOrder = 0
        }else{
            nbDishToOrder -= 1
        }
        nbDishLabel.text = String(nbDishToOrder)
        print(nbDishToOrder)
    }
    
    
    
    
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
